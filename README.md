# Valid Sentence Verifier

This repository hosts a Visual Basic 6.0.0 solution for a coding challenge that focuses on verifying the validity of sentences according to specific rules. The project is developed in Visual Studio 2022.

## Challenge Overview

The core task is to assess whether a string qualifies as a "valid" sentence based on these criteria:
- Begins with a capital letter.
- Contains an even number of quotation marks.
- Ends with one of these characters: ".", "?", or "!".
- Features no period characters other than the last character.
- Numbers below 13 are spelled out.

## Project Structure

- `SentenceValidator.vb`: Main module implementing the sentence validation logic.
- `Utils.vb`: Contains helper functions supporting the main validation logic.
- `Tests.vb`: Unit tests to ensure the reliability of the validation logic under various scenarios.

## Setup and Execution

Follow these steps to set up and run the project:
1. Clone or download the repository to your local machine.
2. Open the project solution file in Visual Studio 2019.
3. Build the solution to compile the code.
4. Execute the application to start the sentence validation process.
5. (Optional) Execute the unit tests in `Tests.vb` for functionality verification.

## Usage

The application prompts for a string input. Enter the sentence you wish to validate, and the program will output whether it adheres to the specified validation rules.
