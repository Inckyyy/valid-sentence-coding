Imports NUnit.Framework
Imports sentence_validation

Namespace sentence_validation_nUnitsTests

    Public Class CheckEndingTests

        Private CheckEndingVar As New CheckEnding


        <SetUp>
        Public Sub Setup()
            CheckEndingVar = New CheckEnding

        End Sub

        <TestCase("Hi how are you")> ' should not pass
        <TestCase("Hi how are you?")> ' should pass
        <TestCase("Hi how are you!")> ' should pass
        <TestCase("Hi how are you.")> ' should pass
        Public Sub validateTest(ByVal TestString As String)

            'Assign

            Dim TestStringArray() As String = TestString.Split(" ")

            '
            'TestString(0) = "Hello"
            'TestString(1) = "how"
            'TestString(2) = "are"
            'TestString(3) = "you?"
            'Act
            Dim result As Boolean = CheckEndingVar.validate(TestStringArray)

            'Setup
            Assert.AreEqual(True, result)


            'Assert.Pass()
        End Sub

    End Class

End Namespace