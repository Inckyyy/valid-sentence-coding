Imports NUnit.Framework
Imports sentence_validation

Namespace sentence_validation_nUnitsTests

    Public Class CheckQuotationsTests

        Private CheckQuotationsVar As New CheckQuotations


        <SetUp>
        Public Sub Setup()
            CheckQuotationsVar = New CheckQuotations

        End Sub

        <TestCase("Hello ""there.""")> ' should pass
        <TestCase("How have you"" been")> ' should not pass
        <TestCase("How are ""things""")> ' should pass
        Public Sub validateTest(ByVal TestString As String)

            'Assign

            Dim TestStringArray() As String = TestString.Split(" ")

            'Act
            Dim result As Boolean = CheckQuotationsVar.validate(TestStringArray)

            'Setup
            Assert.AreEqual(True, result)


            'Assert.Pass()
        End Sub

    End Class

End Namespace