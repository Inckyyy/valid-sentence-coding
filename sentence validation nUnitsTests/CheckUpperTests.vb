Imports NUnit.Framework
Imports sentence_validation

Namespace sentence_validation_nUnitsTests

    Public Class CheckUpperTests

        Private CheckUpperVar As New CheckUpper


        <SetUp>
        Public Sub Setup()
            CheckUpperVar = New CheckUpper

        End Sub

        <TestCase("Hello there.")> ' should pass
        <TestCase("how have you been")> ' should not pass
        <TestCase("How are Things")> ' should pass, definition provided allows for more than one capital letter as long as the first letter of the first word is capitalised
        Public Sub validateTest(ByVal TestString As String)

            'Assign

            Dim TestStringArray() As String = TestString.Split(" ")

            'Act
            Dim result As Boolean = CheckUpperVar.validate(TestStringArray)

            'Setup
            Assert.AreEqual(True, result)


            'Assert.Pass()
        End Sub

    End Class

End Namespace