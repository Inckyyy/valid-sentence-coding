Imports NUnit.Framework
Imports sentence_validation

Namespace sentence_validation_nUnitsTests

    Public Class CheckNumsTests

        Private CheckNumsVar As New CheckNums


        <SetUp>
        Public Sub Setup()
            CheckNumsVar = New CheckNums

        End Sub

        <TestCase("One Two three")> ' should not pass
        <TestCase("1 2 3")> ' should not pass
        <TestCase("Theres 37 sheep")> ' should pass
        <TestCase("Number 11")> ' should not pass
        Public Sub validateTest(ByVal TestString As String)

            'Assign

            Dim TestStringArray() As String = TestString.Split(" ")

            '
            'TestString(0) = "Hello"
            'TestString(1) = "how"
            'TestString(2) = "are"
            'TestString(3) = "you?"
            'Act
            Dim result As Boolean = CheckNumsVar.validate(TestStringArray)

            'Setup
            Assert.AreEqual(True, result)


            'Assert.Pass()
        End Sub

    End Class

End Namespace