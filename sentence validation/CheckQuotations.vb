﻿Public Class CheckQuotations
    Implements IStringValidator

    Public Function validate(TestString() As String) As Boolean Implements IStringValidator.validate
        Dim counter As Integer = 0


        For i = 0 To TestString.Count - 1
            For j = 1 To TestString(i).Length
                If Mid(TestString(i), j, 1) = Chr(34) Then
                    counter += 1
                End If
            Next 'checks each character in every word and if it finds a quotation mark the counter is increased by 1
        Next

        If counter Mod 2 = 0 Then
            Return True
        End If ' if there's an even number of quotation marks it returns true

        Return False
    End Function
End Class
