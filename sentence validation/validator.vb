﻿Public Class validator

    Private TestString() As String
    Private validationList As ArrayList = New ArrayList()

    Sub New(ByVal StringToCheck As string)
        TestString = StringToCheck.Split(" ")
        'breaks the string into a string array, with each element being a word in the string


        Dim NumCheck As New CheckNums()
        validationList.Add(NumCheck)

        Dim EndCheck As New CheckEnding()
        validationList.Add(EndCheck)

        Dim PeriodCheck As New CheckPeriods()
        validationList.Add(PeriodCheck)

        Dim QuoteCheck As New CheckQuotations()
        validationList.Add(QuoteCheck)

        Dim UpperCheck As New CheckUpper()
        validationList.Add(UpperCheck)

    End Sub


    Public Function validate()

        For i = 0 To validationList.Count - 1
            If validationList(i).validate(TestString) = False Then
                Return False
            End If
        Next
        ' as soon as one validation fail happens, returns false without having to check any of the others


        Return True
    End Function


End Class
