﻿Public Class CheckPeriods

    Implements IStringValidator

    Public Function validate(TestString() As String) As Boolean Implements IStringValidator.validate
        Dim counter As Integer = 0



        For i = 0 To TestString.Count - 1
            For j = 1 To TestString(i).Length
                If Mid(TestString(i), j, 1) = (".") Then
                    counter += 1
                End If
            Next
        Next ' for every letter of every word if it, is a period the counter is increased by one

        If counter > 1 Then
            Return False
        End If ' a sentence can only have one period


        If counter = 1 And Mid(TestString(TestString.Count - 1), TestString(TestString.Count - 1).Length, 1) = (".") Then
            Return True
        End If ' if there is a period, it checks if it's at the end of the sentence, which it should be

        Return False
    End Function
End Class
