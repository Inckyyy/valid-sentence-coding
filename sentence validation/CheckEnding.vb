﻿Public Class CheckEnding
    Implements IStringValidator

    Public Function validate(TestString() As String) As Boolean Implements IStringValidator.validate
        Dim WordToCheck As String = TestString(TestString.Count - 1)


        If Mid(WordToCheck, WordToCheck.Length, 1) = (".") Or Mid(WordToCheck, WordToCheck.Length, 1) = ("?") Or Mid(WordToCheck, WordToCheck.Length, 1) = ("!") Then
            Return True
        End If ' checks if the final character is a correct character to end on

        Return False
    End Function
End Class
