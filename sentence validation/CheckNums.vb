﻿Public Class CheckNums
    Implements IStringValidator

    Public Function validate(ByVal TestString() As String) As Boolean Implements IStringValidator.validate
        Dim counter As Integer = 0

        For i = 0 To TestString.Count - 1
            For j = 1 To TestString(i).Length - 1

                If Mid(TestString(i), j, 1) Like "[123456789]" Then
                    Return False
                End If

            Next ' checks for ever number between 1 to 9

            For j = 1 To TestString(i).Length - 2

                If Mid(TestString(i), j, 2) Like "1[0123]" Then
                    Return False
                End If

            Next ' checks for ever number between 10 to 13, these take up more characters
        Next

        Return True
    End Function

End Class
