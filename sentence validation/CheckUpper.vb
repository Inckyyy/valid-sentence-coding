﻿Public Class CheckUpper
    Implements IStringValidator

    Public Function validate(TestString() As String) As Boolean Implements IStringValidator.validate
        Dim WordToCheck As String = TestString(0)


        If Asc(Mid(WordToCheck, 1, 1)) >= Asc("A") And Asc(Mid(WordToCheck, 1, 1)) <= Asc("Z") Then
            Return True
        End If ' checks if the first character is between A and Z on the ascii table which are all the capital letters

        Return False
    End Function
End Class
